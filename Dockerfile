FROM tomcat:7

ADD tomcat-users.xml /usr/local/tomcat/conf/
RUN apt-get update -y
RUN apt-get upgrade -y

COPY target/datetimewebapp-1.0.war /usr/local/tomcat/webapps/app.war

EXPOSE 8080

CMD ["catalina.sh", "run"]

